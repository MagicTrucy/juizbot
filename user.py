class User:

    def __init__(self, bot, nick):
        self.bot = bot
        self.nick = nick.strip('+%@&~')
        self.authenticated = False
        self.user_asked_authentication = False
        self.notes = []

    def __str__(self):
        return self.nick + (' is authenticated' if self.authenticated else '')\
            + (' asked for auth' if self.user_asked_authentication else '')

    def __repr__(self):
        return self.__str__()

    def authenticate(self, user_asked=False):
        '''Send a WHOIS command, and store if the user explicitly asked it.
        If they didn't, don't notify them'''
        if not self.authenticated:
            self.bot.connection.whois(self.nick)
            self.user_asked_authentication = user_asked
        else:
            self.bot.connection.notice(
                    self.nick,
                    "You're already authenticated.")

    def authenticate_response(self, response):
        '''Upon nickserv answer, check if the user is authenticated. If the
        user explicitly asked for an authentication, notify them'''
        if response:
            if not self.authenticated and self.user_asked_authentication:
                self.bot.connection.notice(
                        self.nick,
                        "You're now authenticated.")
            self.authenticated = True
        else:
            if self.user_asked_authentication:
                self.bot.connection.notice(
                        self.nick,
                        "You're not authenticated. " +
                        "Try again when you're authenticated with nickserv")

    def add_note(self, note):
        '''Add a note which will be sent to the user when they're not away'''
        self.notes.append(note)

    def send_all_notes(self):
        '''Send all the notes to the user'''
        for note in self.notes:
            self.bot.connection.notice(self.nick, note)
