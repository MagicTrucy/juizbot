# JuizBot #
An IRC bot with a memo service.

### What does she do? ###
Right now, she just joins chans and check if users are authenticated to nickserv.

A user can ask the bot to check them again with !auth.

### How do I get set up? ###
Get python 3

pip install irc

Edit the configuration file

python3 juizbot.py

### Contribution guidelines ###
Don't actively contribute for now. I learn Python with this project.

But if you want to give me pointers, feel free to mail me at firstname.lastname@gmail.com.
My first name is Erwan.

Better yet, join us on \#dev on the irc.nobleme.com network :)

### Special thanks ###
To Exirel for giving me pointers.

\#dev from irc.nobleme.com in general

### Why Juiz? ###

It's a reference to an anime (Eden of the East (or Higashi no Eden for the purists (or 東のエデン for the japanese speakers))).

She's an AI who provides conciergerie services to the 12 seleção (chosen one in portugese). She can provide ANY service (yes, it does include killing someone).

Here, she only offers a memo service, due to legal (and technical) reasons.