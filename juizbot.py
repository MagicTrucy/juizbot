import argparse
import ssl
from user import User

import config
from irc import bot, connection


def real_nick(nick):
    '''Strip IRC decorator symbols from ``nick``.'''
    if not nick:
        return ''
    return nick.strip('+%@&~')


class JuizBot(bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port):
        '''Handle SSL connection'''
        conn_wrapper = connection.identity
        if config.ssl:
            conn_wrapper = ssl.wrap_socket

        super().__init__(
            [(server, port)],
            nickname,
            nickname,
            connect_factory=connection.Factory(wrapper=conn_wrapper))

        '''Bot custom attributes'''
        self.authenticated_users = {}

    def on_join(self, c, e):
        '''Whenever a user joins a chan, add them to the dictionary'''
        nick = real_nick(e.source.nick)
        if nick not in self.authenticated_users:
            self.authenticated_users[nick] = User(self, nick)
            self.authenticated_users[nick].authenticate()

    def on_welcome(self, c, e):
        '''When the JuizBot is connected to the server'''
        self.authentify(c, e)
        c.join(config.channel)
        '''307 is the code associated to nickserv response events
        http://www.mirc.com/rfc2812.html'''
        c.add_global_handler("307", self.on_nickserv_response)
        c.add_global_handler("namreply", self.on_namreply)
        if config.debug:
            c.add_global_handler("all_events", self.debug)

    def on_part(self, c, e):
        '''When someone leaves the chan, remove them'''
        self.remove_user(real_nick(e.source.nick))

    def on_kick(self, c, e):
        '''When someone gets kicked, remove them'''
        self.remove_user(real_nick(e.arguments[0]))

    def on_quit(self, c, e):
        '''When someone leaves the server, remove them'''
        self.remove_user(real_nick(e.source.nick))

    def on_disconnect(self, c, e):
        '''When someone disconnects, remove them'''
        self.remove_user(real_nick(e.source.nick))

    def debug(self, c, e):
        '''Just dump whatever comes'''
        print(vars(e))

    def on_privnotice(self, c, e):
        pass

    def on_privmsg(self, c, e):
        pass

    def on_pubmsg(self, c, e):
        '''When receiving a message on a chan'''
        self.actions(e.arguments[0], real_nick(e.source.nick))
        self.unaway_user(e.source.nick)

    def on_namreply(self, c, e):
        '''When receiving a list of NAMES, add them to the dictionary
        and try to authenticate them'''
        for nick in e.arguments[2].split():
            nick = real_nick(nick)
            if nick not in self.authenticated_users:
                self.authenticated_users[nick] = User(self, nick)
            self.authenticated_users[nick].authenticate()

    def on_nickserv_response(self, c, e):
        '''Catches nickserv's response'''
        nick = e.arguments[0]
        registered = e.arguments[1].startswith(config.nickserv_identified)
        self.authenticated_users[nick].authenticate_response(registered)

    def authentify(self, c, e):
        '''Authentifies JuizBot'''
        nickserv_auth = config.nickserv_command + " " \
            + config.nickserv_password
        self.connection.privmsg(config.nickserv_nick, nickserv_auth)

    def actions(self, msg, nick):
        '''A list of actions depending on the message received'''
        msg = msg.split()
        if msg[0] == "!auth":
            if nick not in self.authenticated_users:
                self.authenticated_users[nick] = User(self, nick)
            self.authenticated_users[nick].authenticate(True)
        if msg[0] == "!debug":
            self.connection.privmsg(config.channel, self.authenticated_users)
        if msg[0] == "!note" and len(msg) >= 3:
            self.store_note(nick, msg[1], " ".join(msg[2:]))
        if msg[0] == "!note" and len(msg) == 1:
            self.authenticated_users[nick].send_all_notes()

    def unaway_user(self, nick):
        pass

    def remove_user(self, nick):
        '''Remove a user'''
        del self.authenticated_users[nick]

    def store_note(self, source, nick, note):
        self.authenticated_users[nick].add_note(source + ": " + note)
        self.connection.notice(source, "\"" + note + "\" sent to " + nick)


def main():
    '''Main loop for the bot'''
    import config
    parser = argparse.ArgumentParser(description='Launch an IRC bot')
    parser.add_argument(
            '--debug',
            help='Activates debug mode',
            action='store_true')
    parser.add_argument(
            '--channel',
            help='Join a specific channel (overrides config)',
            action='store',
            type=str)
    parser.add_argument(
            '--server',
            help='Join a specific server (overrides config)',
            action='store',
            type=str)
    parser.add_argument(
            '--port',
            help='Connect through a specific port (overrides config)',
            action='store',
            type=int)
    parser.add_argument(
            '--nick',
            help='Choose a nick (overrides config)',
            action='store',
            type=str)
    parser.add_argument(
            '--ssl',
            help='Connect with SSL (overrides config)',
            action='store_true')
    parser.add_argument(
            '--no-ssl',
            help='Connect without SSL (overrides config)',
            action='store_false')
    args = parser.parse_args()
    config.debug = args.debug
    if args.channel is not None:
        config.channel = args.channel
    if args.server is not None:
        config.server = args.server
    if args.port is not None:
        config.port = args.port
    if args.nick is not None:
        config.nickname = args.nick
    bot = JuizBot(config.channel, config.nickname, config.server, config.port)
    print("Starting bot")
    bot.start()

if __name__ == "__main__":
    main()
